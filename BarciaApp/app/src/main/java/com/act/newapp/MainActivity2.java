package com.act.newapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity2 extends AppCompatActivity {

    Button next2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        next2=(Button)findViewById(R.id.btn2);
        next2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                Intent next = new Intent(MainActivity2.this, MainActivity3.class);
                startActivity(next);
            }

        });
    }

    public void onClick(View view) {
    }
}